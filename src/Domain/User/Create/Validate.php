<?php

namespace ASPTest\Domain\User\Create;

use Exception;

class Validate
{
    public function execute(array $data)
    {
        $this->firstNameMinimun2Characters($data['first_name']);
        $this->lastNameMinimun2Characters($data['last_name']);
        $this->firstNameMaximum35Characters($data['first_name']);
        $this->lastNameMaximum35Characters($data['last_name']);
        $this->validEmail($data['email']);

        if (isset($data['age'])) {
            $this->positiveAge($data['age']);
            $this->ageUpTo4Digits($data['age']);
            $this->ageOver150($data['age']);
        }
    }

    private function firstNameMinimun2Characters(string $firstName)
    {
        if (strlen($firstName) < 2) {
            throw new Exception('First name must be at least 2 characters long.');
        }
    }

    private function lastNameMinimun2Characters(string $lastName)
    {
        if (strlen($lastName) < 2) {
            throw new Exception('Last name must be at least 2 characters long.');
        }
    }

    private function firstNameMaximum35Characters(string $firstName)
    {
        if (strlen($firstName) > 35) {
            throw new Exception('The first name must be a maximum of 35 characters.');
        }
    }

    private function lastNameMaximum35Characters(string $lastName)
    {
        if (strlen($lastName) > 35) {
            throw new Exception('The last name must be a maximum of 35 characters.');
        }
    }

    private function validEmail(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('The email is not valid.');
        }
    }

    private function positiveAge(string $age)
    {
        if ($age < 0) {
            throw new Exception('Age cannot be negative.');
        }
    }

    private function ageUpTo4Digits(string $age)
    {
        if (strlen($age) > 4) {
            throw new Exception('Age cannot be greater than four digits.');
        }
    }

    private function ageOver150(string $age)
    {
        if ($age > 150) {
            throw new Exception('Age cannot be over 150.');
        }
    }
}

<?php

namespace ASPTest\Domain\User\Create;

use ASPTest\Database\Entity\User;

class Create
{
    private User $user;
    private Validate $validate;

    public function __construct()
    {
        $this->user = new User();
        $this->validate = new Validate();
    }

    public function execute(array $data): string
    {
        $this->validate->execute($data);
        $lastInsertId = $this->user->create($data);
        $newUser = $this->user->findById($lastInsertId);
        return json_encode($newUser, JSON_PRETTY_PRINT);
    }
}

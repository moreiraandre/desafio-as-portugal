<?php

namespace ASPTest\Domain\User\CreatePassword;

use ASPTest\Database\Entity\User;

class Create
{
    private User $user;
    private Validate $validate;

    public function __construct()
    {
        $this->user = new User();
        $this->validate = new Validate();
    }

    public function execute(int $id, array $data): void
    {
        $this->validate->execute($data);
        $encryptedPassword = $this->encryptPassword($data['password']);
        $this->user->updateById($id, ['password' => $encryptedPassword]);
    }

    private function encryptPassword(string $password): string
    {
        // The 'salt' option is no longer supported.
        $options = [
            'cost' => 10,
        ];

        return password_hash($password, PASSWORD_BCRYPT, $options);
    }
}

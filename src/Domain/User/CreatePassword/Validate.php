<?php

namespace ASPTest\Domain\User\CreatePassword;

use Exception;

class Validate
{
    public function execute(array $data)
    {
        $this->passwordEqualsConfirmation($data);
        $this->passwordMinimum6Characters($data['password']);
        $this->passwordContainEspecialCharacter($data['password']);
        $this->passwordContainNumber($data['password']);
        $this->passwordContainUppercaseLetter($data['password']);
    }

    private function passwordEqualsConfirmation(array $data)
    {
        if ($data['password'] != $data['password_confirmation']) {
            throw new Exception('Password is different from confirmation.');
        }
    }

    private function passwordMinimum6Characters(string $password)
    {
        if (strlen($password) < 6) {
            throw new Exception('Password must be at least 6 characters long.');
        }
    }

    private function passwordContainEspecialCharacter(string $password)
    {
        $regexEspecialCharacter = '/[\'^£$%&*()}{@#~?><>,|=_+¬-]/';
        if (!preg_match($regexEspecialCharacter, $password)) {
            throw new Exception('The password must contain at least 1 special character.');
        }
    }

    private function passwordContainNumber(string $password)
    {
        $regexNumber = '~[0-9]+~';
        if (!preg_match($regexNumber, $password)) {
            throw new Exception('The password must contain at least 1 number.');
        }
    }

    private function passwordContainUppercaseLetter(string $password)
    {
        $regexUppercaseLetter = '/[A-Z]/';
        if (!preg_match($regexUppercaseLetter, $password)) {
            throw new Exception('The password must contain at least 1 uppercase letter.');
        }
    }
}

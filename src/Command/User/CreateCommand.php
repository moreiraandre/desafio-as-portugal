<?php

namespace ASPTest\Command\User;

use ASPTest\Domain\User\Create\Create as UserCreate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    protected static $defaultName = 'user:create';
    protected static $defaultDescription = 'Creates a new user.';

    private UserCreate $userCreate;

    public function __construct()
    {
        parent::__construct();
        $this->userCreate = new UserCreate();
    }

    protected function configure()
    {
        $this->addArgument('first_name', InputArgument::REQUIRED, 'User first name.');
        $this->addArgument('last_name', InputArgument::REQUIRED, 'User last name.');
        $this->addArgument('email', InputArgument::REQUIRED, 'User email.');
        $this->addArgument('age', InputArgument::OPTIONAL, 'User age.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $newUserJson = $this->createUser($input->getArguments());
        $output->writeln('User created successfully.');
        $output->writeln($newUserJson);

        return Command::SUCCESS;
    }

    private function createUser(array $data): string
    {
        array_shift($data); // Remove name of command
        return $this->userCreate->execute($data);
    }
}

<?php

namespace ASPTest\Command\User;

use ASPTest\Domain\User\CreatePassword\Create as UserPasswordCreate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreatePasswordCommand extends Command
{
    protected static $defaultName = 'user:create-pwd';
    protected static $defaultDescription = 'Set user password.';

    private UserPasswordCreate $userPasswordCreate;

    public function __construct()
    {
        parent::__construct();
        $this->userPasswordCreate = new UserPasswordCreate();
    }

    protected function configure()
    {
        $this->addArgument('id', InputArgument::REQUIRED, 'User id.');
        $this->addArgument('password', InputArgument::REQUIRED, 'User password.');
        $this->addArgument('password_confirmation', InputArgument::REQUIRED, 'User password confirmation.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getArgument('id');
        $data = $input->getArguments();

        $this->userPasswordCreate->execute($userId, $data);
        $output->writeln('User password set successfully.');

        return Command::SUCCESS;
    }
}

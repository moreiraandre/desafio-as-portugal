<?php

namespace ASPTest\Database;

use PDO;
use PDOException;

class Connection
{
    private ?PDO $connection = null;

    public function __construct()
    {
        $host = 'mysql';
        $dbname = 'desafio';
        $username = 'root';
        $password = 'root';

        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}

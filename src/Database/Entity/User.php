<?php

namespace ASPTest\Database\Entity;

use ASPTest\Database\Connection;
use Exception;
use PDO;

class User
{
    private Connection $connection;
    private string $table = 'user';
    private string $primaryKey = 'user_id';

    public function __construct()
    {
        $this->connection = new Connection();
    }

    public function findById(int $id): ?object
    {
        $consulta = $this
            ->connection
            ->getConnection()
            ->query("SELECT * FROM {$this->table} WHERE {$this->primaryKey} = $id;");

        $row = $consulta->fetch(PDO::FETCH_OBJ);
        if ($row) {
            return $row;
        }

        return null;
    }

    public function deleteById(int $id): void
    {
        $query = $this
            ->connection
            ->getConnection()
            ->prepare("DELETE FROM {$this->table} WHERE {$this->primaryKey} = $id;");

        $query->execute();
    }

    public function updateById(int $id, array $data): void
    {
        $foundUser = $this->findById($id);
        if (!$foundUser) {
            throw new Exception('User not found.');
        }

        $columns = $this->updatePrepareColumns($data);

        $query = $this
            ->connection
            ->getConnection()
            ->prepare("UPDATE {$this->table} SET $columns WHERE {$this->primaryKey} = $id;");

        $query->execute($data);
    }

    private function updatePrepareColumns(array $data): string
    {
        $columns = array_keys($data);
        $columns = array_map(fn($column) => "$column=:$column", $columns);
        return implode(',', $columns);
    }

    public function create(array $data): int
    {
        $tableColumns = $this->createPrepareTableColumns($data);
        $valuesColumns = $this->createPrepareValuesColumns($data);

        $query = $this
            ->connection
            ->getConnection()
            ->prepare("INSERT INTO {$this->table}($tableColumns) VALUES($valuesColumns);");

        $query->execute($data);

        return $this
            ->connection
            ->getConnection()
            ->lastInsertId();
    }

    private function createPrepareTableColumns(array $data): string
    {
        $tableColumns = array_keys($data);
        return implode(',', $tableColumns);
    }

    private function createPrepareValuesColumns(array $data): string
    {
        $valuesColumns = array_keys($data);
        $valuesColumns = array_map(fn($column) => ":$column", $valuesColumns);
        return implode(',', $valuesColumns);
    }
}

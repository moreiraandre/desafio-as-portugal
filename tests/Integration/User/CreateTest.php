<?php

namespace Tests\Integration\User;

use ASPTest\Domain\User\Create\Create;
use PHPUnit\Framework\TestCase;
use ASPTest\Database\Entity\User as Entity;

class CreateTest extends TestCase
{
    public function testSuccessWithAge()
    {
        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
            'age' => '32',
        ];

        $domain = new Create();
        $newUserJson = $domain->execute($data);

        $newUserObject = json_decode($newUserJson);
        $data['user_id'] = $newUserObject->user_id;
        $data['password'] = null;

        $this->assertJsonStringEqualsJsonString($newUserJson, json_encode($data));

        $entity = new Entity();
        $entity->deleteById($data['user_id']);
    }

    public function testSuccessWithoutAge()
    {
        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
        ];

        $domain = new Create();
        $newUserJson = $domain->execute($data);

        $newUserObject = json_decode($newUserJson);
        $data['user_id'] = $newUserObject->user_id;
        $data['password'] = null;
        $data['age'] = null;

        $this->assertJsonStringEqualsJsonString($newUserJson, json_encode($data));

        $entity = new Entity();
        $entity->deleteById($data['user_id']);
    }

    public function testFailureFirstNameMinimun2Characters()
    {
        $this->expectExceptionMessage('First name must be at least 2 characters long.');

        $data = [
            'first_name' => 'J',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureLastNameMinimun2Characters()
    {
        $this->expectExceptionMessage('Last name must be at least 2 characters long.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'R',
            'email' => 'john.robert@gmail.com',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureFirstNameMaximum35Characters()
    {
        $this->expectExceptionMessage('The first name must be a maximum of 35 characters.');

        $moreThan35Characters = str_pad('', 36, 'A');
        $data = [
            'first_name' => $moreThan35Characters,
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureLastNameMaximum35Characters()
    {
        $this->expectExceptionMessage('The last name must be a maximum of 35 characters.');

        $moreThan35Characters = str_pad('', 36, 'A');
        $data = [
            'first_name' => 'John',
            'last_name' => $moreThan35Characters,
            'email' => 'john.robert@gmail.com',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureValidEmailOnlyUser()
    {
        $this->expectExceptionMessage('The email is not valid.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureValidEmailWithoutTheAtSign()
    {
        $this->expectExceptionMessage('The email is not valid.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robertgmail.com',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureValidEmailWithoutDomain()
    {
        $this->expectExceptionMessage('The email is not valid.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureValidEmailWithoutDomainRegulativeEntity()
    {
        $this->expectExceptionMessage('The email is not valid.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailurePositiveAge()
    {
        $this->expectExceptionMessage('Age cannot be negative.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
            'age' => '-30',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureAgeUpTo4Digits()
    {
        $this->expectExceptionMessage('Age cannot be greater than four digits.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
            'age' => '12345',
        ];

        $domain = new Create();
        $domain->execute($data);
    }

    public function testFailureAgeOver150()
    {
        $this->expectExceptionMessage('Age cannot be over 150.');

        $data = [
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
            'age' => '151',
        ];

        $domain = new Create();
        $domain->execute($data);
    }
}
<?php

namespace Tests\Integration\User;

use ASPTest\Domain\User\Create\Create as UserCreate;
use ASPTest\Domain\User\CreatePassword\Create;
use PHPUnit\Framework\TestCase;
use ASPTest\Database\Entity\User as Entity;

class CreatePasswordTest extends TestCase
{
    public function testSuccess()
    {
        $userCreate = new UserCreate();
        $newUserJson = $userCreate->execute([
            'first_name' => 'John',
            'last_name' => 'Robert',
            'email' => 'john.robert@gmail.com',
        ]);
        $newUserObject = json_decode($newUserJson);
        $newUserId = $newUserObject->user_id;

        $password = 'abc@1A';
        $data = [
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);

        $entity = new Entity();
        $foundUser = $entity->findById($newUserId);

        $this->assertTrue(password_verify($password, $foundUser->password));

        $entity->deleteById($newUserId);
    }

    public function testFailurePasswordEqualsConfirmation()
    {
        $this->expectExceptionMessage('Password is different from confirmation.');

        $newUserId = 0;

        $data = [
            'password' => 'a',
            'password_confirmation' => 'b',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }

    public function testFailurePasswordMinimum6Characters()
    {
        $this->expectExceptionMessage('Password must be at least 6 characters long.');

        $newUserId = 0;

        $data = [
            'password' => 'a',
            'password_confirmation' => 'a',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }

    public function testFailurePasswordContainEspecialCharacter()
    {
        $this->expectExceptionMessage('The password must contain at least 1 special character.');

        $newUserId = 0;

        $data = [
            'password' => '123456',
            'password_confirmation' => '123456',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }

    public function testFailurePasswordContainNumber()
    {
        $this->expectExceptionMessage('The password must contain at least 1 number.');

        $newUserId = 0;

        $data = [
            'password' => 'abcde@',
            'password_confirmation' => 'abcde@',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }

    public function testFailurePasswordContainUppercaseLetter()
    {
        $this->expectExceptionMessage('The password must contain at least 1 uppercase letter.');

        $newUserId = 0;

        $data = [
            'password' => 'abcd@1',
            'password_confirmation' => 'abcd@1',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }

    public function testFailureUserIdNotFound()
    {
        $this->expectExceptionMessage('User not found.');

        $newUserId = 0;

        $data = [
            'password' => 'abc@1A',
            'password_confirmation' => 'abc@1A',
        ];

        $domain = new Create();
        $domain->execute($newUserId, $data);
    }
}
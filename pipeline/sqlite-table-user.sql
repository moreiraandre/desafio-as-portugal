CREATE TABLE user
(
    user_id    INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name VARCHAR(35) NOT NULL,
    last_name  VARCHAR(35) NOT NULL,
    email      VARCHAR(50) NOT NULL,
    age        INT,
    password   VARCHAR(255)
);
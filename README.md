# Desafio Alter Solutions PHP
Este desafio consiste na criação de dois serviços CLI utilizando a linguagem PHP, um para `criação de usuário` e outro para `definição de senha`.

# Requisitos
- [Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Docker
Inicie o MySql
```bash
docker-compose up -d
```
> Na primeira execução será criada automaticamente a tabela `user` no banco `desafio`.

Acesse o terminal do container `composer` para poder utilizar o `Composer`, `PHPUnit`, executar os serviços do desafio, etc.
```bash
docker-compose run composer sh
```

Caso precise finalizar o MySql utilize:
```bash
docker-compose down
```
> Você poderá acessar o banco em um client externo com os dados que estão no arquivo [Connection.php](https://gitlab.com/moreiraandre/desafio-as-portugal/-/blob/test/src/Database/Connection.php) trocando apenas o `host` para `localhost`.

## Instalando dependências Composer
Após acessar o terminal do container `composer` execute:
```bash
composer install
```
> As pastas `vendor` e `binvendor` serão criadas na raiz do projeto.

## Utilizando os serviços
Após acessar o terminal do container `composer`, execute os comandos abaixo substituindo os parâmetros pelos valores desejados:

### Criar usuário
```bash
php bin/console user:create first_name last_name email age
```
Parâmetros

| Parâmetro  | Descrição     | Obrigatorio | 
|------------|---------------|-------------|
| first_name | Primeiro nome | X           |
| last_name  | Último nome   | X           |
| email      | Email         | X           |
| age        | Idade         |             |

### Definir senha do usuário
```bash
php bin/console user:create-pwd user_id password password_confirmation
```
Parâmetros

| Parâmetro             | Descrição            | Obrigatorio | 
|-----------------------|----------------------|-------------|
| user_id               | Id do usuário        | X           |
| password              | Senha                | X           |
| password_confirmation | Confirmação da senha | X           |

## Testes
```bash
XDEBUG_MODE=coverage binvendor/phpunit --debug --verbose --coverage-text --coverage-html coverage
```
> A pasta `coverage` será criada na raiz do projeto, ela conterá o relatório em html de cobertura de testes no código, você poderá acessar abrindo o arquivo `coverage/index.html` no seu navegador.

## Tecnologias utilizadas
* Docker
* MySql 5.7
* PHP 8.0
* [Symfony Console](https://symfony.com/doc/current/console.html)
* [PHPUnit](https://phpunit.readthedocs.io/pt_BR/latest/)

## Contato
André Moreira <andre.mcx1@gmail.com>
* LinkedIn https://www.linkedin.com/in/andr%C3%A9-moreira-78a27b160/
* GitLab https://gitlab.com/moreiraandre

## Extras
Faça uma visita ao meu [canal do YouTube](https://www.youtube.com/channel/UCwF0hY7IMgvTAoopy-_fF1w) e confira meu minicurso de API com Laravel, não esqueça de se inscrever e deixar o seu like pra dar uma força :)